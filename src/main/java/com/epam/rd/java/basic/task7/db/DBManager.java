package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {

    private static final String SQL_INSERT_USER = "INSERT INTO users VALUES (DEFAULT, ?)";
    private static final String SQL_FIND_ALL_USERS = "SELECT * FROM users";
    private static final String SQL_INSERT_TEAMS = "INSERT INTO teams VALUES (DEFAULT, ?)";
    private static final String SQL_FIND_ALL_TEAMS = "SELECT * FROM teams";
    private static final String GET_USER_FROM_TABLE = "SELECT * FROM users WHERE login = ?";
    private static final String GET_TEAM_FROM_TABLE = "SELECT * FROM teams WHERE name = ?";
    private static final String INSERT_TEAMS_FOR_USER = "INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)";
    private static final String FIND_USER_TEAMS = "SELECT * FROM users_teams WHERE user_id = ?";
    private static final String FIND_TEAM_BY_ID = "SELECT * FROM teams WHERE id = ?";
    private static final String DELETE_TEAM = "DELETE FROM teams WHERE id = ?";
    private static final String UPDATE_TEAMS = "UPDATE teams SET name = ? WHERE id = ?";
    private static final String DELETE_USERS = "DELETE FROM users WHERE id = ?";

    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
    }

    public List<User> findAllUsers() throws DBException {

        List<User> list = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(getURL());
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL_USERS);
        ) {
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                list.add(mapUser(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;

    }

    public boolean insertUser(User user) throws DBException {
        try (Connection con = DriverManager.getConnection(getURL());
             PreparedStatement statement = con.prepareStatement(SQL_INSERT_USER, Statement.RETURN_GENERATED_KEYS);
        ) {
            statement.setString(1, user.getLogin());
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("No rows affected");
            }
            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    user.setId(generatedKeys.getInt(1));
                } else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;

    }

    public boolean deleteUsers(User... users) throws DBException {
        for (User user : users) {
            if (user == null) {
                return false;
            }
            try (Connection con = DriverManager.getConnection(getURL());
                 PreparedStatement stmt = con.prepareStatement(DELETE_USERS);
            ) {
                stmt.setInt(1, user.getId());
                stmt.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;

    }

    public User getUser(String login) throws DBException {
        User user = User.createUser(login);
        try (Connection con = DriverManager.getConnection(getURL());
             PreparedStatement stmt = con.prepareStatement(GET_USER_FROM_TABLE);
        ) {
            stmt.setString(1, login);
            ResultSet resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                user.setId(resultSet.getInt("id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;

    }

    public Team getTeam(String name) throws DBException {
        Team team = Team.createTeam(name);
        try (Connection con = DriverManager.getConnection(getURL());
             PreparedStatement stmt = con.prepareStatement(GET_TEAM_FROM_TABLE);
        ) {
            stmt.setString(1, name);
            ResultSet resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                team.setId(resultSet.getInt("id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return team;

    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> list = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(getURL());
             PreparedStatement statement = con.prepareStatement(SQL_FIND_ALL_TEAMS);
        ) {
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                list.add(mapTeam(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;


    }

    public boolean insertTeam(Team team) throws DBException {

        try (Connection con = DriverManager.getConnection(getURL());
             PreparedStatement statement = con.prepareStatement(SQL_INSERT_TEAMS, Statement.RETURN_GENERATED_KEYS);
        ) {
            statement.setString(1, team.getName());
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("No rows affected");
            }
            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    team.setId(generatedKeys.getInt(1));
                } else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;

    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        if (user == null) {
            throw new DBException("", new NullPointerException());
        }
        try (Connection con = DriverManager.getConnection(getURL());)
        {
            con.setAutoCommit(false);
            try (PreparedStatement statement = con.prepareStatement(INSERT_TEAMS_FOR_USER)) {
                for (Team team : teams) {
                    if (team == null) {
                        con.rollback();
                        throw new DBException("", new NullPointerException());
                    }
                    statement.setString(1, String.valueOf(user.getId()));
                    statement.setString(2, String.valueOf(team.getId()));
                    statement.executeUpdate();
                }
            } catch (SQLException e) {
                con.rollback();
                throw new DBException("", e);
            }
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException(" ", e);
        }

        return true;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> list = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(getURL());
             PreparedStatement statement = con.prepareStatement(FIND_USER_TEAMS);
        ) {
            statement.setInt(1, user.getId());
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                list.add(mapTeamByTeamID(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;

    }

    public boolean deleteTeam(Team team) throws DBException {
        if (team == null) {
            return false;
        }
        try (Connection con = DriverManager.getConnection(getURL());
             PreparedStatement stmt = con.prepareStatement(DELETE_TEAM);
        ) {
            stmt.setInt(1, team.getId());
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        return true;

    }

    public boolean updateTeam(Team team) throws DBException {
        try (Connection con = DriverManager.getConnection(getURL());
             PreparedStatement statement = con.prepareStatement(UPDATE_TEAMS);
        ) {
            statement.setString(1, team.getName());
            statement.setInt(2, team.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;

    }

    //////////////////////////////////////////////////////////////////

    private String getURL() {
        String URL = null;
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("app.properties"));
            URL = properties.getProperty("connection.url");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return URL;
    }

    private Team mapTeamByTeamID(ResultSet rs) throws SQLException {
        try (Connection con = DriverManager.getConnection(getURL());
             PreparedStatement statement = con.prepareStatement(FIND_TEAM_BY_ID);
        ) {
            statement.setInt(1, rs.getInt("team_id"));
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return mapTeam(resultSet);
        }
    }

    private Team mapTeam(ResultSet rs) throws SQLException {
        Team team = Team.createTeam(rs.getString("name"));
        team.setId(rs.getInt("id"));
        return team;
    }

    private User mapUser(ResultSet rs) throws SQLException {
        User user = User.createUser(rs.getString("login"));
        user.setId(rs.getInt("id"));
        return user;
    }

    //////////////////////////////////////////////////////////////////

}
